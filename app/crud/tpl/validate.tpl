<?php
/**
 * @copyright Copyright (c) 2023 水月居工作室
 * @license https://opensource.org/licenses/Apache-2.0
 * @link https://www.shuiyueju.com
 */

namespace app\admin\validate;
use think\Validate;

class <model>Validate extends Validate
{
    protected $rule = <rule>;

    protected $message = <message>;
}