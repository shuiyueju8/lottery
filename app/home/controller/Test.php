<?php
/**
 * @copyright Copyright (c) 2021 勾股工作室
 * @license https://opensource.org/licenses/Apache-2.0
 * @link https://www.gougucms.com
 */

//declare (strict_types = 1);

namespace app\home\controller;

use app\home\BaseController;

use think\facade\View;
use think\facade\Db;
use think\facade\Env;
use think\faced\Filesystem;
use think\exception\ValidateException;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Shared\Date as Shared;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;

class Test extends BaseController
{
    //测试列表2
    public function index2(){
        $Temperature = array(666,'0','666',null);

        foreach ($Temperature as $key => $value) {
             echo ($value);
            if(is_numeric($value)){


            print("参数是数字或数字字符串<br/>");
            }else{

            print("参数不是数字或数字字符串<br/>");
            }
        }
        exit();
        $res=Db::name('ChengjiStScore')->find(9264);
        dump($res);
        $sub=['语文'=>98,'数学'=>96];
        $js_sub=json_encode($sub);
        dump($js_sub);

        $js=json_encode($res['subject_score']);
        dump($js);
        $subjects=json_decode($res['subject_score'],true);
        dump($subjects);

    }
    /**
 * 批量上传班级信息
 * */
 public function import_class()
 {
    //$param = get_params();


    //$kid=request()->param('kid');
    $kid=18;

    if(empty($kid)){
        return to_assign(1, '导入失败，未找到kid');
    }else{
        $kaoshi_info=Db::name('ChengjiKaoshi')->find($kid);
    }
//^ array:11 [▼
//   "id" => 18
//   "term" => "2023A"
//   "year" => 2023
//   "kaoshi_date" => "2023-11-21"
//   "title_s" => "2023上"
//   "title" => "2023学年第1学期期中考试"
//   "update_time" => 1701233946
//   "create_time" => 1700804272
//   "subject" => "语文,数学,科学,英语"
//   "uid" => 1
//   "status" => 1
// ]
    $files[]= request()->file('fileToUpload');
    // dump($files);die;
    //return to_assign(0, '导入成功');
    

       
        // 验证文件大小，名称等是否正确
        validate(['xlsfile'=>'filesiz:10*1024*1024|fileExt:xls,xlsx'])
            ->check($files);
        $savename = \think\facade\Filesystem::disk('public')->putFile('xls', $files[0]);
        // dump($savename);
        
        $fileExtendName = substr(strrchr($savename, '.'), 1);
            // 有Xls和Xlsx格式两种
            if ($fileExtendName == 'xlsx') {
                $objReader = IOFactory::createReader('Xlsx');
            } else {
                $objReader = IOFactory::createReader('Xls');
            }
            $objReader->setReadDataOnly(TRUE);
            $path = get_config('filesystem.disks.public.url');
            // dump($path);die;
            // 读取文件，tp6默认上传的文件，在runtime的相应目录下，可根据实际情况自己更改
            $flilename='.'.$path . '/' .$savename;

            $objPHPExcel = $objReader->load('.'.$path . '/' .$savename);

            $sheet = $objPHPExcel->getSheet(0);   //excel中的第一张sheet
            $highestRow = $sheet->getHighestRow();       // 取得总行数
            $highestColumn = $sheet->getHighestColumn();   // 取得总列数
            Coordinate::columnIndexFromString($highestColumn);
            $lines = $highestRow - 1;
            if ($lines <= 0) {
                return to_assign(1, '数据不能为空');
                exit();
            }

// dump($sheet);die;

            for ($j = 2; $j <= $highestRow; $j++) {
                //班级    学号  学生姓名    性别  课程ID    手机  学生志愿1   学生志愿2   备注

                $bj_name=$objPHPExcel->getActiveSheet()->getCell("A" . $j)->getValue();
                $st_code=$objPHPExcel->getActiveSheet()->getCell("B" . $j)->getValue();
                $bj_code=substr($st_code, 0, strlen($st_code)-2);
                if(strlen($bj_code)==4){$bj_code='20'.$bj_code;}
                 $grade_array=['未知','一年级','二年级','三年级','四年级','五年级','六年级'];
                 $grade_char = mb_substr($bj_name, 0, 3);
                 $grade=array_search($grade_char,$grade_array);//年级
                $st_name=$objPHPExcel->getActiveSheet()->getCell("C" . $j)->getValue();
                $sex =$objPHPExcel->getActiveSheet()->getCell("D" . $j)->getValue();
                $data[$j - 2] = [
                    'term'    =>$kaoshi_info['term'],
                    'kid'     =>$kaoshi_info['id'],
                    'bj_name' =>$bj_name,
                    'grade'   =>$grade,
                    'bj_code' =>$bj_code,
                    'st_code' => $st_code,
                    'st_name' =>$st_name, 
                    'sex'     => $sex,
                    'status'  => 1,
                    'update_time'=>time(),
                    'create_time' =>time(),
                   
                ];
                // dd($data);die;

            }
            // dump($data);die;

            // 批量添加数据
            $count = Db::name('ChengjiStScore')->insertAll($data);
            if ($count) {
                return to_assign(0, '导入成功, 共导入学生数据'.$count.'条');
            }
            else{
                return to_assign(1, '导入失败，请检查excel文件再试');
            }

}


    public function index()
    {


        if(request()->isPost()){
            $param = get_params();
    
        $kid=$param['kid'];
        $grades=$param['grades'];
       // dump($param);die;
        $where=[];

        $info=json_encode($param['grades']);
        return $grades;
       // dump($param);die;
        }else{
             return view();
        }

           
       
    }
    public function dosum(){
        if(request()->isPost()){
        $param = get_params();    
        $kid=$param['kid'];
        $grades=$param['grades'];
        dump($param['grades']);
        var_dump($param);die;
    }
    }
    public function index3()
    {
         return view();
    }

    //上传文件
    public function uploader(){
         //$file[]= request()->file('file');
         // 日期前綴
         $dataPath = date('Ym');
         
         //$savename = \think\facade\Filesystem::disk('public')->putFile($dataPath, $file[0], 'md5');
          $file = request()->file('file');  //这里‘file’是你提交时的name
          $md5 = $file->md5();
    try{
        // validate(['xlsFile' => [     //goodFile是你自定义的键名，目的是为了对check里数组中的
        //     'fileSize' =>50*1024*1024,//goodFile字段值进行验证;允许文件大小
        //     'fileExt'  => array('xls','xlxs'),  //允许的文件后缀
        //     ]])->check(['xlsFile'=>$file]);//也就是对上传的$file进行验证
        $saveName = \think\facade\Filesystem::disk('public')->putFile($dataPath, $file);//保存文件名
        //$arr = ['status' => 200, 'msg' => '成功', 'path' => app()->getRootPath().'public/storage'.$saveName];
        $arr = ['status' => 200, 'msg' => '成功', 'path' => 'public/storage/'.$saveName];
        return json($arr); //返回标准json格式
    }catch (\Exception $e) {
        return $this->exceptionHandle($e,'上传失败!' . $e->getMessage(),'json','');
    }
    }
}
