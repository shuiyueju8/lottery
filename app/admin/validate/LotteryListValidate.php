<?php
/**
 * @copyright Copyright (c) 2021 勾股工作室
 * @license https://opensource.org/licenses/Apache-2.0
 * @link https://www.gougucms.com
 */

namespace app\admin\validate;
use think\Validate;

class LotteryListValidate extends Validate
{
    protected $rule = [
    'real_name' => 'require',
    'phone_number' => 'require',
    'level' => 'require',
    'lottery_code' => 'require',
    'got_award' => 'require',
    'ctime' => 'require',
];

    protected $message = [
    'real_name.require' => '姓名不能为空',
    'phone_number.require' => '手机号码不能为空',
    'level.require' => '获奖等级不能为空',
    'lottery_code.require' => '兑奖号不能为空',
    'got_award.require' => '是否已领奖不能为空',
    'ctime.require' => '中奖时间不能为空',
];
}