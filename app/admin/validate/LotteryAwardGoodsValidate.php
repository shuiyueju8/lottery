<?php
/**
 * @copyright Copyright (c) 2021 勾股工作室
 * @license https://opensource.org/licenses/Apache-2.0
 * @link https://www.gougucms.com
 */

namespace app\admin\validate;
use think\Validate;

class LotteryAwardGoodsValidate extends Validate
{
    protected $rule = [
    'award_level' => 'require',
    'winner_number' => 'require',
];

    protected $message = [
    'award_level.require' => '奖次不能为空',
    'winner_number.require' => '奖品数量不能为空',
];
}