<?php
/**
 * @copyright Copyright (c) 2021 勾股工作室
 * @license https://opensource.org/licenses/Apache-2.0
 * @link https://www.gougucms.com
 */

namespace app\admin\validate;
use think\Validate;

class LotteryConfigValidate extends Validate
{
    protected $rule = [
    'title' => 'require',
    'name' => 'require',
    'content' => 'require',
];

    protected $message = [
    'title.require' => '标题不能为空',
    'name.require' => '名称不能为空',
    'content.require' => '配置内容不能为空',
];
}