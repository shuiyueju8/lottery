<?php
/**
 * @copyright Copyright (c) 2021 勾股工作室
 * @license https://opensource.org/licenses/Apache-2.0
 * @link https://www.gougucms.com
 */

namespace app\admin\validate;
use think\Validate;

class FileDownloadValidate extends Validate
{
    protected $rule = [
    'filename' => 'require',
    'filepath' => 'require',
    'fileext' => 'require',
];

    protected $message = [
    'filename.require' => '文件名不能为空',
    'filepath.require' => '文件路径+文件名不能为空',
    'fileext.require' => '文件后缀不能为空',
];
}