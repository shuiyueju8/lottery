<?php
/**
 * @copyright Copyright (c) 2021 勾股工作室
 * @license https://opensource.org/licenses/Apache-2.0
 * @link https://www.gougucms.com
 */

namespace app\admin\validate;
use think\Validate;

class LotteryStaffValidate extends Validate
{
    protected $rule = [
    'real_name' => 'require',
    'status' => 'require',
    'department' => 'require',
    // 'signed' => 'require',
    
];

    protected $message = [
    'real_name.require' => '姓名不能为空',
    'status.require' => '类别不能为空',
    'department.require' => '部门不能为空',
    // 'signed.require' => '是否签到不能为空',
    
];
}