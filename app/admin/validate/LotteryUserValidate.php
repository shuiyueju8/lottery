<?php
/**
 * @copyright Copyright (c) 2021 勾股工作室
 * @license https://opensource.org/licenses/Apache-2.0
 * @link https://www.gougucms.com
 */

namespace app\admin\validate;
use think\Validate;

class LotteryUserValidate extends Validate
{
    protected $rule = [
    'phone_number' => 'require',
    'real_name' => 'require',
    'sign_time' => 'require',
    'avatar_url' => 'require',
    'status' => 'require',
    'lottery_allow' => 'require',
    'lottery_done' => 'require',
];

    protected $message = [
    'phone_number.require' => '手机号码不能为空',
    'real_name.require' => '姓名不能为空',
    'sign_time.require' => '签到时间不能为空',
    'avatar_url.require' => '头像url不能为空',
    'status.require' => '状态（员工、嘉宾）不能为空',
    'lottery_allow.require' => '允许抽奖：1；否则：0不能为空',
    'lottery_done.require' => '是否已经抽奖不能为空',
];
}