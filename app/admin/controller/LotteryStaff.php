<?php
/**
 * @copyright Copyright (c) 2021 勾股工作室
 * @license https://opensource.org/licenses/Apache-2.0
 * @link https://www.gougucms.com
 */
 
declare (strict_types = 1);

namespace app\admin\controller;

use app\admin\BaseController;
use app\admin\model\LotteryStaff as LotteryStaffModel;
use app\admin\validate\LotteryStaffValidate;
use think\exception\ValidateException;
use think\facade\Db;
use think\facade\View;

class LotteryStaff extends BaseController

{
	/**
     * 构造函数
     */
    public function __construct()
    {
        $this->model = new LotteryStaffModel();
		$this->uid = get_login_admin('id');
    }
    /**
    * 数据列表
    */
    public function datalist()
    {
        if (request()->isAjax()) {
			$param = get_params();
			$where = [];
			if (!empty($param['keywords'])) {
                $where[] = ['real_name|phone_number|level', 'like', '%' . $param['keywords'] . '%'];
            }
            $list = $this->model->getLotteryStaffList($where,$param);
            return table_assign(0, '', $list);
        }
        else{
            return view();
        }
    }
    /**
     *  手动签到
     */
   
    public function sign(){
        $param = get_params();
        $id = isset($param['id']) ? $param['id'] : 0;
        
        $staff=$this->model->find($id);
        if($staff['signed']=='1'){
           return to_assign('0','该员工已签到，无需再次签到！'); 
           exit();
        }
        if(!empty($staff)){
            $this->model->where(['id'=>$id])->save(['signed'=>'1']);
            $number=rand(1,16);//取1至16的随机数为头像
            //dd($number);
            $data=[
                'real_name'=>$staff['real_name'],
                'phone_number'=>$staff['phone_number'],
                'avatar_url'=>'/avatar/'.$number.'.png',
                'sign_time'=>date('Y-m-d H:i:s',time()),
                'status'=>$staff['status'],
                'lottery_allow'=>1,
                'lottery_done'=>0
            ];
            $res=Db::name('LotteryUser')->save($data);
            // dd($data);
            if($res){
              return to_assign('0','手动签到成功！');
            }
        }
    }
    /**
     *  取消签到
     */
   
    public function unsign(){
        $param = get_params();
        $id = isset($param['id']) ? $param['id'] : 0;
        if(empty($id)){
            return to_assign('0','员工id错误！'); 
           exit();
        }
        $staff=$this->model->find($id);
        if($staff['signed']=='1'){
           $this->model->where(['id'=>$id])->save(['signed'=>'0']); 
           $res=Db::name('LotteryUser')->where(['real_name'=>$staff['real_name']])->delete();
           return to_assign('0','该员工已取消签到！'); 
          
        }

    }

    /**
    * 添加
    */
    public function add()
    {
        if (request()->isAjax()) {		
			$param = get_params();	
			
            // 检验完整性
            try {
                validate(LotteryStaffValidate::class)->check($param);
            } catch (ValidateException $e) {
                // 验证失败 输出错误信息
                return to_assign(1, $e->getError());
            }
			
            $this->model->addLotteryStaff($param);
        }else{
			return view();
		}
    }
	

    /**
    * 编辑
    */
    public function edit()
    {
		$param = get_params();
		
        if (request()->isAjax()) {			
            // 检验完整性
            try {
                validate(LotteryStaffValidate::class)->check($param);
            } catch (ValidateException $e) {
                // 验证失败 输出错误信息
                return to_assign(1, $e->getError());
            }
			
            $this->model->editLotteryStaff($param);
        }else{
			$id = isset($param['id']) ? $param['id'] : 0;
			$detail = $this->model->getLotteryStaffById($id);
			if (!empty($detail)) {
				View::assign('detail', $detail);
				return view();
			}
			else{
				throw new \think\exception\HttpException(404, '找不到页面');
			}			
		}
    }


    /**
    * 查看信息
    */
    public function read()
    {
        $param = get_params();
		$id = isset($param['id']) ? $param['id'] : 0;
		$detail = $this->model->getLotteryStaffById($id);
		if (!empty($detail)) {
			View::assign('detail', $detail);
			return view();
		}
		else{
			throw new \think\exception\HttpException(404, '找不到页面');
		}
    }

    /**
    * 删除
	* type=0,逻辑删除，默认
	* type=1,物理删除
    */
    public function del()
    {
        $param = get_params();
		$id = isset($param['id']) ? $param['id'] : 0;
		$type = isset($param['type']) ? $param['type'] : 0;

        $this->model->delLotteryStaffById($id,$type);
   }
}
