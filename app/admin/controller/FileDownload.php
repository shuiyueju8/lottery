<?php
/**
 * @copyright Copyright (c) 2021 勾股工作室
 * @license https://opensource.org/licenses/Apache-2.0
 * @link https://www.gougucms.com
 */
 
declare (strict_types = 1);

namespace app\admin\controller;

use app\admin\BaseController;
use app\admin\model\FileDownload as FileDownloadModel;
use app\admin\validate\FileDownloadValidate;
use think\exception\ValidateException;
use think\facade\Db;
use think\facade\View;

class FileDownload extends BaseController

{
	/**
     * 构造函数
     */
    public function __construct()
    {
        $this->model = new FileDownloadModel();
		$this->uid = get_login_admin('id');
    }
   /**
    * 下载文件
    */
    public function download(){
        $id=request()->param('id');
        $file=$this->model->find($id)->toArray();
       
        // 获取要下载的文件路径
        $file_path = $file['filepath'];
        
        // 检查文件是否存在
        if (file_exists($file_path)) {
           //文件下载数量更新+1;
            $file['download']++;        
            $this->model->where('id',$file['id'])->update(['download'=>$file['download']]);

            // 设置响应头，告诉浏览器下载文件
            ob_end_clean();
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($file_path));
            header('Content-Length: '.filesize($file_path));            
            // 读取文件内容并发送到客户端
            readfile($file_path);

        }else{
            // 文件不存在，抛出异常
            abort(404, '文件不存在');
        }
    }
    /**
    * 数据列表
    */
    public function datalist()
    {
        if (request()->isAjax()) {
			$param = get_params();
			$where = [];
			$where[] = ["delete_time","=",0];
            $list = $this->model->getFileDownloadList($where,$param);
            return table_assign(0, '', $list);
        }
        else{
            return view();
        }
    }

    /**
    * 添加
    */
    public function add()
    {
        if (request()->isAjax()) {		
			$param = get_params();	
			
            // 检验完整性
            try {
                validate(FileDownloadValidate::class)->check($param);
            } catch (ValidateException $e) {
                // 验证失败 输出错误信息
                return to_assign(1, $e->getError());
            }
			
            $this->model->addFileDownload($param);
        }else{
			return view();
		}
    }
	

    /**
    * 编辑
    */
    public function edit()
    {
		$param = get_params();
		
        if (request()->isAjax()) {			
            // 检验完整性
            try {
                validate(FileDownloadValidate::class)->check($param);
            } catch (ValidateException $e) {
                // 验证失败 输出错误信息
                return to_assign(1, $e->getError());
            }
			
            $this->model->editFileDownload($param);
        }else{
			$id = isset($param['id']) ? $param['id'] : 0;
			$detail = $this->model->getFileDownloadById($id);
			if (!empty($detail)) {
				View::assign('detail', $detail);
				return view();
			}
			else{
				throw new \think\exception\HttpException(404, '找不到页面');
			}			
		}
    }


    /**
    * 查看信息
    */
    public function read()
    {
        $param = get_params();
		$id = isset($param['id']) ? $param['id'] : 0;
		$detail = $this->model->getFileDownloadById($id);
		if (!empty($detail)) {
			View::assign('detail', $detail);
			return view();
		}
		else{
			throw new \think\exception\HttpException(404, '找不到页面');
		}
    }

    /**
    * 删除
	* type=0,逻辑删除，默认
	* type=1,物理删除
    */
    public function del()
    {
        $param = get_params();
		$id = isset($param['id']) ? $param['id'] : 0;
		$type = isset($param['type']) ? $param['type'] : 0;

        $this->model->delFileDownloadById($id,$type);
   }
}
