<?php
/**
 * @copyright Copyright (c) 2023 水月居工作室
 * @license https://opensource.org/licenses/Apache-2.0
 * @link https://www.ttxwgyxx.cn
 */

declare (strict_types = 1);

namespace app\admin\controller;

use app\admin\BaseController;
use think\facade\Db;
use think\facade\View;

class Lottery extends BaseController
{
    public function index()
    {
        add_user_log('view', '首页');
		$count = \think\facade\Db::name('UserLog')->where(array('type' => 'down'))->count();
        return View('',['count'=>$count]);
    }

    /**
     * 前端抽奖平台等待开始按钮的post请求时,返回全部候选用户,用于滚动
     */
    public function lotteryPageAllSignUser(){
        //加上嘉宾和未知人员        
        $map['lottery_done']=0;//头像滚动只显示未中奖的
        $map['lottery_allow']=1;//允许抽奖的员工
        $res = Db::name('LotteryUser')->where($map)->select()->toArray();
        // dd($res);
        shuffle($res);
        echo json_encode($res);
    }
    /** 前端抽奖平台开始按钮的post请求,返回对应等级的抽奖用户结果.
    *   从数据库抽取用户后,在签到用户表做相应标记,不允许此轮用户进入下一轮抽奖.
    *   在中奖者表记录中奖人和生成兑奖号,并发送短信
    */
    public function lotteryNowLevel(){
        //$configModel=M('config');
        $nowLevel=Db::name('LotteryConfig')->where(['name'=>'now_level'])->value('content');
         // dd($nowLevel);
        echo $nowLevel;
    }
     /**
     * 进入抽奖页面
     */
    public function lottery_page(){

        $nowLevel =Db::name('LotteryConfig')->where(['name'=>'now_level'])->value('content');
        $level=Db::name('LotteryAwardGoods')->where(['award_level'=>$nowLevel])->find();
        
         // dump($level);die;
        $award_list = Db::name('LotteryAwardGoods')->order('id')->select();
        // dd($award_list);
        View::assign([
            'level'   => $level,
            'awardList' => $award_list
        ] );
        return View('start_lottery');
    }

    /**
     * 抽取获奖人员
     * */
	public function lotteryPageBegin(){
        header("Cache-Control: no-cache, must-revalidate");
        //$configModel=M('config');
        $nowLevel=Db::name('LotteryConfig')->where(['name'=>'now_level'])->value('content');
        $nowLevel =trim($nowLevel);

        if($nowLevel!==""){
             //$awardModel =M('Award_goods');
             $levelNum=Db::name('LotteryAwardGoods')->where(['award_level'=>$nowLevel])->value('winner_number');//抽奖数量
             $limitNum=Db::name('LotteryAwardGoods')->where(['award_level'=>$nowLevel])->value('limit_number');//单次抽奖限定人数
             // dd($limitNum);
             $lotteryNum=$levelNum>$limitNum ? $limitNum : $levelNum;//抽奖总人数超过每次抽奖的限制人数则抽取单次设置人数 
             //剩余抽奖数大于0，则执行抽奖 
             if($levelNum>0) {
                $res = Db::query('SELECT * FROM wgy_lottery_user WHERE lottery_allow=1 and lottery_done=0   ORDER BY rand() LIMIT '.$lotteryNum);
                shuffle($res);
                echo json_encode($res);
                //抽取用户后,设置是否已经参加了抽奖....若数据库的符合条件的用户数小于抽奖预设数量???
                $doneNum =count($res);                
                for ($i = 0; $i< $doneNum; $i++) {
                    $condition['id'] = $res[$i]['id'];
                    $data['lottery_done'] = 1;

                    $getUserList=Db::name('LotteryUser')->where($condition)->save($data);////抽奖后标记好,不进入下一轮
                   // dump($getUserList); die;
                    if($getUserList){
                        //更新该用户在全部员工表中的等级状态                        
                        $map['real_name']=$res[$i]['real_name'];
                        $data4['level']=$nowLevel;
                        Db::name('LotteryStaff')->where($map)->save($data4);//在员工表中写入获奖等级
                        //逐个写入中奖表
                        $data1['real_name']=$res[$i]['real_name'];
                        $data1['phone_number']=$res[$i]['phone_number'];
                        $data1['level']=$nowLevel;
                        $data1['lottery_code']='2024'.$res[$i]['id'];
                        $data1['got_award']='否';//默认还没领奖
                        $data1['ctime']=date('Y-m-d H:i:s',time());
                        //$lotteryList=M('lottery_list');
                        $re =Db::name('LotteryList')->save($data1); 
                        $winner_number=Db::name('LotteryAwardGoods')->where('award_level',$nowLevel)->dec('winner_number')->update();//抽奖人数减一                       
                    }
                   
                }
             }            

        }else{
            $this->error('非法操作!','?s=/Admin/Index/lotteryPage',2);
        }
    }
                                    


}
