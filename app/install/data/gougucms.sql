/**
 * @copyright Copyright (c) 2021 勾股工作室
 * @license https://opensource.org/licenses/Apache-2.0
 * @link https://www.gougucms.com
 */


SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `cms_admin`
-- ----------------------------
DROP TABLE IF EXISTS `cms_admin`;
CREATE TABLE `cms_admin` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL DEFAULT '',
  `pwd` varchar(100) NOT NULL DEFAULT '',
  `salt` varchar(100) NOT NULL DEFAULT '',
  `nickname` varchar(255) DEFAULT '',
  `thumb` varchar(255) DEFAULT NULL,
  `theme` varchar(255) NOT NULL DEFAULT 'black' COMMENT '系统主题',
  `mobile` bigint(11) DEFAULT '0',
  `email` varchar(255) DEFAULT '',
  `desc` text COMMENT '备注',
  `did` int(11) NOT NULL DEFAULT 0 COMMENT '部门id',
  `position_id` int(11) NOT NULL DEFAULT 0 COMMENT '职位id',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL DEFAULT '0',
  `last_login_time` int(11) NOT NULL DEFAULT '0',
  `login_num` int(11) NOT NULL DEFAULT '0',
  `last_login_ip` varchar(64) NOT NULL DEFAULT '',
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '1正常,0禁止登录,-1删除',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARACTER SET = utf8mb4 COMMENT='管理员表';

-- ----------------------------
-- Table structure for cms_admin_rule
-- ----------------------------
DROP TABLE IF EXISTS `cms_admin_rule`;
CREATE TABLE `cms_admin_rule`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父id',
  `src` varchar(255) NOT NULL DEFAULT '' COMMENT 'url链接',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '名称',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '日志操作名称',
  `icon` varchar(255) NOT NULL DEFAULT '' COMMENT '图标',
  `menu` int(1) NOT NULL DEFAULT 0 COMMENT '是否是菜单,1是,2不是',
  `sort` int(11) NOT NULL DEFAULT 1 COMMENT '越小越靠前',
  `status` int(1) NOT NULL DEFAULT 1 COMMENT '状态,0禁用,1正常',
  `module` varchar(255) NOT NULL DEFAULT '' COMMENT '所属模块',
  `crud` varchar(255) NOT NULL DEFAULT '' COMMENT 'crud标识',
  `create_time` int(11) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 163 CHARACTER SET = utf8mb4 COMMENT = '菜单及权限表';

-- ----------------------------
-- Records of cms_admin_rule
-- ----------------------------
INSERT INTO `cms_admin_rule` VALUES (1, 0, '', '系统管理', '系统管理', 'ri-settings-3-line', 1, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (2, 0, '', '系统工具', '系统工具', 'ri-list-settings-line', 1, 2, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (3, 0, '', '基础数据', '基础数据', 'ri-database-2-line', 1, 3, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (4, 0, '', '平台用户', '平台用户', 'ri-group-line', 1, 4, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (5, 0, '', '资讯中心', '资讯中心', 'ri-article-line', 1, 5, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (6, 0, '', '图集中心', '图集中心', 'ri-image-2-line', 1, 6, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (7, 0, '', '商品中心', '商品中心', 'ri-shopping-bag-3-line', 1, 7, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (8, 0, '', '单 页 面', '单 页 面', 'ri-file-list-3-line', 1, 8, 1,'','', 0, 0);

INSERT INTO `cms_admin_rule` VALUES (9, 1, 'admin/conf/index', '系统配置', '系统配置', '', 1, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (10, 9, 'admin/conf/add', '新建/编辑', '配置项', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (11, 9, 'admin/conf/delete', '删除', '配置项', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (12, 9, 'admin/conf/edit', '编辑', '配置详情', '', 2, 1, 1,'','', 0, 0);

INSERT INTO `cms_admin_rule` VALUES (13, 1, 'admin/module/index', '功能模块', '功能模块', '', 1, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (14, 13, 'admin/module/install', '安装', '功能模块', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (15, 13, 'admin/module/upgrade', '升级', '功能模块', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (16, 13, 'admin/module/uninstall', '卸载', '功能模块', '', 2, 1, 1,'','', 0, 0);

INSERT INTO `cms_admin_rule` VALUES (17, 1, 'admin/rule/index', '功能节点', '功能节点', '', 1, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (18, 17, 'admin/rule/add', '新建/编辑', '功能节点', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (19, 17, 'admin/rule/delete', '删除', '功能节点', '', 2, 1, 1,'','', 0, 0);

INSERT INTO `cms_admin_rule` VALUES (20, 1, 'admin/role/index', '权限角色', '权限角色', '', 1, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (21, 20, 'admin/role/add', '新建/编辑', '权限角色', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (22, 20, 'admin/role/delete', '删除', '权限角色', '', 2, 1, 1,'','', 0, 0);

INSERT INTO `cms_admin_rule` VALUES (23, 1, 'admin/department/index', '部门架构', '部门', '', 1, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (24, 23, 'admin/department/add', '新建/编辑', '部门', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (25, 23, 'admin/department/delete', '删除', '部门', '', 2, 1, 1,'','', 0, 0);

INSERT INTO `cms_admin_rule` VALUES (26, 1, 'admin/position/index', '岗位职称', '岗位职称', '', 1, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (27, 26, 'admin/position/add', '新建/编辑', '岗位职称', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (28, 26, 'admin/position/delete', '删除', '岗位职称',  '', 2, 1, 1,'','', 0, 0);

INSERT INTO `cms_admin_rule` VALUES (29, 1, 'admin/admin/index', '系统用户', '系统用户', '', 1, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (30, 29, 'admin/admin/add', '添加/修改', '系统用户', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (31, 29, 'admin/admin/view', '查看', '系统用户', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (32, 29, 'admin/admin/delete', '删除', '系统用户', '', 2, 1, 1,'','', 0, 0);

INSERT INTO `cms_admin_rule` VALUES (33, 2, 'admin/log/index', '操作日志', '操作日志', '', 1, 1, 1,'','', 0, 0);

INSERT INTO `cms_admin_rule` VALUES (34, 2, 'admin/crud/index', '一键CRUD', 'CRUD代码生成', '', 1, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (35, 34, 'admin/crud/table', 'CRUD查看', 'CRUD查看', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (36, 34, 'admin/crud/crud', 'CRUD操作', 'CRUD代码生成', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (37, 34, 'admin/crud/menu', '生成菜单', '菜单生成', '', 2, 1, 1,'','', 0, 0);

INSERT INTO `cms_admin_rule` VALUES (38, 2, 'admin/database/database', '备份数据', '数据备份', '', 1, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (39, 38, 'admin/database/backup', '备份数据表', '数据', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (40, 38, 'admin/database/optimize', '优化数据表', '数据表', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (41, 38, 'admin/database/repair', '修复数据表', '数据表', '', 2, 1, 1,'','', 0, 0);

INSERT INTO `cms_admin_rule` VALUES (42, 2, 'admin/database/backuplist', '还原数据', '数据还原', '', 1, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (43, 42, 'admin/database/import', '还原数据表', '数据', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (44, 42, 'admin/database/downfile', '下载备份数据', '备份数据', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (45, 42, 'admin/database/del', '删除备份数据', '备份数据', '', 2, 1, 1,'','', 0, 0);

INSERT INTO `cms_admin_rule` VALUES (46, 2, 'admin/files/index', '附件管理','附件管理', '', 1, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (47, 46, 'admin/files/edit', '编辑附件','附件', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (48, 46, 'admin/files/move', '移动附件','附件', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (49, 46, 'admin/files/delete', '删除附件','附件', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (50, 46, 'admin/files/get_group', '附件分组','附件分组', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (51, 46, 'admin/files/add_group', '新建/编辑','附件分组', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (52, 46, 'admin/files/del_group', '删除附件分组','附件分组', '', 2, 1, 1,'','', 0, 0);

INSERT INTO `cms_admin_rule` VALUES (53, 3, 'admin/nav/index', '导航设置','导航组', '', 1, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (54, 53, 'admin/nav/add', '新建/编辑','导航组', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (55, 53, 'admin/nav/delete', '删除','导航组', '', 2, 1, 1,'','', 0, 0);

INSERT INTO `cms_admin_rule` VALUES (56, 3, 'admin/nav/nav_info', '导航管理','导航', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (57, 56, 'admin/nav/nav_info_add', '新建/编辑','导航', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (58, 56, 'admin/nav/nav_info_delete', '删除导航','导航', '', 2, 1, 1,'','', 0, 0);

INSERT INTO `cms_admin_rule` VALUES (59, 3, 'admin/sitemap/index', '网站地图','网站地图分类', '', 1, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (60, 59, 'admin/sitemap/add', '新建/编辑','网站地图分类', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (61, 59, 'admin/sitemap/delete', '删除','网站地图分类', '', 2, 1, 1,'','', 0, 0);

INSERT INTO `cms_admin_rule` VALUES (62, 3, 'admin/sitemap/sitemap_info', '网站地图管理','网站地图', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (63, 62, 'admin/sitemap/sitemap_info_add', '新建/编辑','网站地图', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (64, 62, 'admin/sitemap/sitemap_info_delete', '删除','网站地图', '', 2, 1, 1,'','', 0, 0);

INSERT INTO `cms_admin_rule` VALUES (65, 3, 'admin/slide/index', '轮播广告','轮播组', '', 1, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (66, 65, 'admin/slide/add', '新建/编辑','轮播组', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (67, 65, 'admin/slide/delete', '删除','轮播组', '', 2, 1, 1,'','', 0, 0);

INSERT INTO `cms_admin_rule` VALUES (68, 3, 'admin/slide/slide_info', '轮播广告管理','轮播图', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (69, 68, 'admin/slide/slide_info_add', '新建/编辑','轮播图', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (70, 68, 'admin/slide/slide_info_delete', '删除','轮播图', '', 2, 1, 1,'','', 0, 0);

INSERT INTO `cms_admin_rule` VALUES (71, 3, 'admin/links/index', '友情链接', '友情链接', '', 1, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (72, 71, 'admin/links/add', '新建/编辑', '友情链接', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (73, 71, 'admin/links/delete', '删除', '友情链接', '', 2, 1, 1,'','', 0, 0);

INSERT INTO `cms_admin_rule` VALUES (74, 3, 'admin/keywords/index', 'SEO关键字','SEO关键字', '', 1, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (75, 74, 'admin/keywords/add', '新建/编辑','SEO关键字', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (76, 74, 'admin/keywords/delete', '删除','SEO关键字', '', 2, 1, 1,'','', 0, 0);

INSERT INTO `cms_admin_rule` VALUES (77, 3, 'admin/search/index', '搜索关键字','搜索关键字', '', 1, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (78, 77, 'admin/search/delete', '删除','搜索关键字', '', 2, 1, 1,'','', 0, 0);

INSERT INTO `cms_admin_rule` VALUES (79, 4, 'admin/level/index', '用户等级', '用户等级', '', 1, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (80, 79, 'admin/level/add', '新建/编辑', '用户等级', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (81, 79, 'admin/level/disable', '禁用/启用', '用户等级', '', 2, 1, 1,'','', 0, 0);

INSERT INTO `cms_admin_rule` VALUES (82, 4, 'admin/user/index', '用户管理','用户', '', 1, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (83, 82, 'admin/user/edit', '编辑','用户信息', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (84, 82, 'admin/user/view', '查看','用户信息', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (85, 82, 'admin/user/disable', '禁用/启用','用户', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (86, 4, 'admin/user/record', '操作记录','用户操作记录', '', 1, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (87, 4, 'admin/user/log', '操作日志','用户操作日志', '', 1, 1, 1,'','', 0, 0);

INSERT INTO `cms_admin_rule` VALUES (97, 6, 'admin/gallery_cate/datalist', '图集分类','图集分类', '', 1, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (98, 97, 'admin/gallery_cate/add', '新建','图集分类', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (99, 97, 'admin/gallery_cate/edit', '编辑','图集分类', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (100, 97, 'admin/gallery_cate/del', '删除','图集分类', '', 2, 1, 1,'','', 0, 0);

INSERT INTO `cms_admin_rule` VALUES (101, 6, 'admin/gallery/datalist', '图集列表','图集', '', 1, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (102, 101, 'admin/gallery/add', '新建','图集', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (103, 101, 'admin/gallery/edit', '编辑','图集', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (104, 101, 'admin/gallery/read', '查看','图集', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (105, 101, 'admin/gallery/del', '删除','图集', '', 2, 1, 1,'','', 0, 0);

INSERT INTO `cms_admin_rule` VALUES (106, 7, 'admin/goods_cate/datalist', '商品分类','商品分类', '', 1, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (107, 106, 'admin/goods_cate/add', '新建','商品分类', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (108, 106, 'admin/goods_cate/edit', '编辑','商品分类', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (109, 106, 'admin/goods_cate/del', '删除','商品分类', '', 2, 1, 1,'','', 0, 0);

INSERT INTO `cms_admin_rule` VALUES (110, 7, 'admin/goods/datalist', '商品列表','商品', '', 1, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (111, 110, 'admin/goods/add', '新建','商品', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (112, 110, 'admin/goods/edit', '编辑','商品', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (113, 110, 'admin/goods/read', '查看','商品', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (114, 110, 'admin/goods/del', '删除','商品', '', 2, 1, 1,'','', 0, 0);

INSERT INTO `cms_admin_rule` VALUES (115, 8, 'admin/pages/datalist', '单页面列表','单页面', '', 1, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (116, 115, 'admin/pages/add', '新建','单页面', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (117, 115, 'admin/pages/edit', '编辑','单页面', '', 2, 1, 1,'','', 0, 0);
INSERT INTO `cms_admin_rule` VALUES (118, 115, 'admin/pages/del', '删除','单页面', '', 2, 1, 1,'','', 0, 0);

INSERT INTO `cms_admin_rule` VALUES (119, 8, 'admin/analysis/index', '智能分析', '智能分析', '', 1, 1, 1,'','', 0, 0);

INSERT INTO `cms_admin_rule` VALUES ('120', '0', '', '抽奖用户管理', '抽奖用户', 'ri-community-line', '1', '9', '1', '', 'lottery_user', '1703681397', '0');
INSERT INTO `cms_admin_rule` VALUES ('121', '120', 'admin/lottery_user/datalist', '抽奖用户列表', '抽奖用户列表', '', '1', '0', '1', '', 'lottery_user', '1703681397', '0');
INSERT INTO `cms_admin_rule` VALUES ('122', '121', 'admin/lottery_user/add', '新建', '抽奖用户', '', '2', '0', '1', '', 'lottery_user', '1703681397', '0');
INSERT INTO `cms_admin_rule` VALUES ('123', '121', 'admin/lottery_user/edit', '编辑', '抽奖用户', '', '2', '0', '1', '', 'lottery_user', '1703681397', '0');
INSERT INTO `cms_admin_rule` VALUES ('124', '121', 'admin/lottery_user/read', '查看', '抽奖用户', '', '2', '0', '1', '', 'lottery_user', '1703681397', '0');
INSERT INTO `cms_admin_rule` VALUES ('125', '121', 'admin/lottery_user/del', '删除', '抽奖用户', '', '2', '0', '1', '', 'lottery_user', '1703681397', '0');
INSERT INTO `cms_admin_rule` VALUES ('126', '0', '', '员工管理', '员工', 'ri-community-fill', '1', '10', '1', '', 'lottery_staff', '1703684062', '0');
INSERT INTO `cms_admin_rule` VALUES ('127', '126', 'admin/lottery_staff/datalist', '员工列表', '员工列表', '', '1', '0', '1', '', 'lottery_staff', '1703684062', '0');
INSERT INTO `cms_admin_rule` VALUES ('128', '127', 'admin/lottery_staff/add', '新建', '员工', '', '2', '0', '1', '', 'lottery_staff', '1703684062', '0');
INSERT INTO `cms_admin_rule` VALUES ('129', '127', 'admin/lottery_staff/edit', '编辑', '员工', '', '2', '0', '1', '', 'lottery_staff', '1703684062', '0');
INSERT INTO `cms_admin_rule` VALUES ('130', '127', 'admin/lottery_staff/read', '查看', '员工', '', '2', '0', '1', '', 'lottery_staff', '1703684062', '0');
INSERT INTO `cms_admin_rule` VALUES ('131', '127', 'admin/lottery_staff/del', '删除', '员工', '', '2', '0', '1', '', 'lottery_staff', '1703684062', '0');
INSERT INTO `cms_admin_rule` VALUES ('132', '127', 'admin/lottery_staff/sign', '手动签到', '手动签到', '', '2', '0', '1', '', '', '1703687501', '0');
INSERT INTO `cms_admin_rule` VALUES ('133', '127', 'admin/lottery_staff/unsign', '取消签到', '取消签到', '', '2', '0', '1', '', '', '1703690225', '0');
INSERT INTO `cms_admin_rule` VALUES ('134', '0', '', '中奖名单管理', '中奖名单', 'ri-store-2-line', '1', '11', '1', '', 'lottery_list', '1703702930', '0');
INSERT INTO `cms_admin_rule` VALUES ('135', '134', 'admin/lottery_list/datalist', '中奖名单列表', '中奖名单列表', '', '1', '0', '1', '', 'lottery_list', '1703702930', '0');
INSERT INTO `cms_admin_rule` VALUES ('136', '135', 'admin/lottery_list/add', '新建', '中奖名单', '', '2', '0', '1', '', 'lottery_list', '1703702930', '0');
INSERT INTO `cms_admin_rule` VALUES ('137', '135', 'admin/lottery_list/edit', '编辑', '中奖名单', '', '2', '0', '1', '', 'lottery_list', '1703702930', '0');
INSERT INTO `cms_admin_rule` VALUES ('138', '135', 'admin/lottery_list/read', '查看', '中奖名单', '', '2', '0', '1', '', 'lottery_list', '1703702930', '0');
INSERT INTO `cms_admin_rule` VALUES ('139', '135', 'admin/lottery_list/del', '删除', '中奖名单', '', '2', '0', '1', '', 'lottery_list', '1703702930', '0');
INSERT INTO `cms_admin_rule` VALUES ('140', '0', '', '奖品设置管理', '奖品设置', 'ri-store-2-line', '1', '12', '1', '', 'lottery_award_goods', '1703703148', '0');
INSERT INTO `cms_admin_rule` VALUES ('141', '140', 'admin/lottery_award_goods/datalist', '奖品设置列表', '奖品设置列表', '', '1', '0', '1', '', 'lottery_award_goods', '1703703148', '0');
INSERT INTO `cms_admin_rule` VALUES ('142', '141', 'admin/lottery_award_goods/add', '新建', '奖品设置', '', '2', '0', '1', '', 'lottery_award_goods', '1703703148', '0');
INSERT INTO `cms_admin_rule` VALUES ('143', '141', 'admin/lottery_award_goods/edit', '编辑', '奖品设置', '', '2', '0', '1', '', 'lottery_award_goods', '1703703148', '0');
INSERT INTO `cms_admin_rule` VALUES ('144', '141', 'admin/lottery_award_goods/read', '查看', '奖品设置', '', '2', '0', '1', '', 'lottery_award_goods', '1703703148', '0');
INSERT INTO `cms_admin_rule` VALUES ('145', '141', 'admin/lottery_award_goods/del', '删除', '奖品设置', '', '2', '0', '1', '', 'lottery_award_goods', '1703703148', '0');
INSERT INTO `cms_admin_rule` VALUES ('146', '141', 'admin/lottery_award_goods/reset', '重置抽奖信息', '重置抽奖', '', '2', '0', '1', '', '', '1703722233', '0');
INSERT INTO `cms_admin_rule` VALUES ('147', '140', 'admin/lottery_award_goods/configlist', '抽奖设置', '抽奖设置', '', '1', '0', '1', '', '', '1703736977', '0');
INSERT INTO `cms_admin_rule` VALUES ('148', '0', '', '抽奖设置管理', '抽奖设置', 'ri-store-2-fill', '1', '13', '1', '', 'lottery_config', '1703738475', '0');
INSERT INTO `cms_admin_rule` VALUES ('149', '148', 'admin/lottery_config/datalist', '抽奖设置列表', '抽奖设置列表', '', '1', '0', '1', '', 'lottery_config', '1703738475', '0');
INSERT INTO `cms_admin_rule` VALUES ('150', '149', 'admin/lottery_config/add', '新建', '抽奖设置', '', '2', '0', '1', '', 'lottery_config', '1703738475', '0');
INSERT INTO `cms_admin_rule` VALUES ('151', '149', 'admin/lottery_config/edit', '编辑', '抽奖设置', '', '2', '0', '1', '', 'lottery_config', '1703738475', '0');
INSERT INTO `cms_admin_rule` VALUES ('152', '149', 'admin/lottery_config/read', '查看', '抽奖设置', '', '2', '0', '1', '', 'lottery_config', '1703738475', '0');
INSERT INTO `cms_admin_rule` VALUES ('153', '149', 'admin/lottery_config/del', '删除', '抽奖设置', '', '2', '0', '1', '', 'lottery_config', '1703738475', '0');
INSERT INTO `cms_admin_rule` VALUES ('154', '0', '', '文件下载管理', '文件下载', 'ri-folder-line', '1', '14', '1', '', 'file_download', '1703770627', '0');
INSERT INTO `cms_admin_rule` VALUES ('155', '154', 'admin/file_download/datalist', '文件下载列表', '文件下载列表', '', '1', '0', '1', '', 'file_download', '1703770627', '0');
INSERT INTO `cms_admin_rule` VALUES ('156', '155', 'admin/file_download/add', '新建', '文件下载', '', '2', '0', '1', '', 'file_download', '1703770627', '0');
INSERT INTO `cms_admin_rule` VALUES ('157', '155', 'admin/file_download/edit', '编辑', '文件下载', '', '2', '0', '1', '', 'file_download', '1703770627', '0');
INSERT INTO `cms_admin_rule` VALUES ('158', '155', 'admin/file_download/read', '查看', '文件下载', '', '2', '0', '1', '', 'file_download', '1703770627', '0');
INSERT INTO `cms_admin_rule` VALUES ('159', '155', 'admin/file_download/del', '删除', '文件下载', '', '2', '0', '1', '', 'file_download', '1703770627', '0');
INSERT INTO `cms_admin_rule` VALUES ('160', '155', 'admin/file_download/download', '下载文件', '下载文件', '', '2', '0', '1', '', '', '1703771211', '0');
INSERT INTO `cms_admin_rule` VALUES ('161', '0', '', '开始抽奖', '抽奖', 'ri-magic-line', '1', '0', '1', '', '', '1704033256', '0');
INSERT INTO `cms_admin_rule` VALUES ('162', '161', 'admin/lottery/lottery_page', '开始抽奖', '抽奖', '', '1', '1', '1', '', '', '1704033764', '0');
-- ----------------------------
-- Table structure for `cms_admin_group`
-- ----------------------------
DROP TABLE IF EXISTS `cms_admin_group`;
CREATE TABLE `cms_admin_group` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `status` int(1) NOT NULL DEFAULT '1',
  `rules` varchar(1000) DEFAULT '' COMMENT '用户组拥有的规则id， 多个规则","隔开',
  `desc` text COMMENT '备注',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARACTER SET = utf8mb4 COMMENT='权限分组表';

-- ----------------------------
-- Records of cms_admin_group
-- ----------------------------
INSERT INTO `cms_admin_group` VALUES ('1', '超级管理员', '1', '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162', '超级管理员，系统自动分配所有可操作权限及菜单。', '0', '1703770627');
INSERT INTO `cms_admin_group` VALUES ('2', '测试管理员', '1', '1,9,13,17,20,23,26,29,31,2,33,34,35,38,42,43,44,45,46,3,53,56,59,62,65,68,71,74,77,4,79,82,84,87,6,97,101,104,7,106,110,113,8,115,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160', '测试角色', '0', '0');

-- ----------------------------
-- Table structure for `cms_admin_group_access`
-- ----------------------------
DROP TABLE IF EXISTS `cms_admin_group_access`;
CREATE TABLE `cms_admin_group_access` (
  `uid` int(11) unsigned DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARACTER SET = utf8mb4 COMMENT='权限分组和管理员的关联表';

-- ----------------------------
-- Records of cms_admin_group_access
-- ----------------------------
INSERT INTO `cms_admin_group_access` VALUES ('1', '1', '0', '0');

-- ----------------------------
-- Table structure for cms_admin_module
-- ----------------------------
DROP TABLE IF EXISTS `cms_admin_module`;
CREATE TABLE `cms_admin_module`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '模块名称',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '模块所在目录，小写字母',
  `icon` varchar(255) NOT NULL DEFAULT '' COMMENT '图标',
  `status` int(1) NOT NULL DEFAULT 1 COMMENT '状态,0禁用,1正常',
  `type` int(1) NOT NULL DEFAULT 2 COMMENT '模块类型,2普通模块,1系统模块',
  `create_time` int(11) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COMMENT = '功能模块表';

-- ----------------------------
-- Records of cms_admin_module
-- ----------------------------
INSERT INTO `cms_admin_module` VALUES (1, '后台模块', 'admin', '', 1, 1, 1639562910, 0);
INSERT INTO `cms_admin_module` VALUES (2, '前台模块', 'home', '', 1, 1, 1639562910, 0);

-- ----------------------------
-- Table structure for `cms_admin_log`
-- ----------------------------
DROP TABLE IF EXISTS `cms_admin_log`;
CREATE TABLE `cms_admin_log`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `uid` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户ID',
  `nickname` varchar(255) NOT NULL DEFAULT '' COMMENT '昵称',
  `type` varchar(80) NOT NULL DEFAULT '' COMMENT '操作类型',
  `action` varchar(80) NOT NULL DEFAULT '' COMMENT '操作动作',
  `subject` varchar(80) NOT NULL DEFAULT '' COMMENT '操作主体',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '操作标题',
  `content` text NULL COMMENT '操作描述',
  `module` varchar(32) NOT NULL DEFAULT '' COMMENT '模块',
  `controller` varchar(32) NOT NULL DEFAULT '' COMMENT '控制器',
  `function` varchar(32) NOT NULL DEFAULT '' COMMENT '方法',
  `rule_menu` varchar(255) NOT NULL DEFAULT '' COMMENT '节点权限路径',
  `ip` varchar(64) NOT NULL DEFAULT '' COMMENT '登录ip',
  `param_id` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '操作数据id',
  `param` text NULL COMMENT '参数json格式',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0删除 1正常',
  `create_time` int(11) NOT NULL DEFAULT 0 COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COMMENT = '后台操作日志表';

-- ----------------------------
-- Table structure for `cms_config`
-- ----------------------------
DROP TABLE IF EXISTS `cms_config`;
CREATE TABLE `cms_config` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '配置名称',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '配置标识',
  `content` text NULL COMMENT '配置内容',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：-1删除 0禁用 1启用',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET = utf8mb4 COMMENT='系统配置表';

-- ----------------------------
-- Records of cms_config
-- ----------------------------
INSERT INTO `cms_config` VALUES (1, '网站配置', 'web', 'a:13:{s:2:\"id\";s:1:\"1\";s:11:\"admin_title\";s:9:\"勾股CMS\";s:5:\"title\";s:9:\"勾股cms\";s:4:\"logo\";s:39:\"/static/admin/images/nonepic360x360.jpg\";s:4:\"file\";s:0:\"\";s:6:\"domain\";s:24:\"https://www.gougucms.com\";s:3:\"icp\";s:23:\"粤ICP备1xxxxxx11号-1\";s:8:\"keywords\";s:9:\"勾股cms\";s:5:\"beian\";s:29:\"粤公网安备1xxxxxx11号-1\";s:4:\"desc\";s:258:\"勾股CMS是一套基于ThinkPHP6 + Layui + MySql打造的轻量级、高性能快速建站的内容管理系统。后台管理模块，一目了然，操作简单，通用型后台权限管理框架，紧随潮流、极低门槛、开箱即用。           \";s:4:\"code\";s:0:\"\";s:9:\"copyright\";s:39:\"© 2022 gougucms.com Apache-2.0 license\";s:7:\"version\";s:6:\"2.0.18\";}', 1, 1612514630, 1645057819);
INSERT INTO `cms_config` VALUES (2, '邮箱配置', 'email', 'a:8:{s:2:\"id\";s:1:\"2\";s:4:\"smtp\";s:11:\"smtp.qq.com\";s:9:\"smtp_port\";s:3:\"465\";s:9:\"smtp_user\";s:15:\"gougucms@qq.com\";s:8:\"smtp_pwd\";s:6:\"123456\";s:4:\"from\";s:24:\"勾股CMS系统管理员\";s:5:\"email\";s:18:\"admin@gougucms.com\";s:8:\"template\";s:122:\"<p>勾股CMS是一套基于ThinkPHP6 + Layui + MySql打造的轻量级、高性能快速建站的内容管理系统。</p>\";}', 1, 1612521657, 1619088538);
INSERT INTO `cms_config` VALUES (3, '微信配置', 'wechat', 'a:11:{s:2:\"id\";s:1:\"3\";s:5:\"token\";s:8:\"GOUGUCMS\";s:14:\"login_back_url\";s:49:\"https://www.gougucms.com/wechat/index/getChatInfo\";s:5:\"appid\";s:18:\"wxdf96xxxx7cd6f0c5\";s:9:\"appsecret\";s:32:\"1dbf319a4f0dfed7xxxxfd1c7dbba488\";s:5:\"mchid\";s:10:\"151xxxx331\";s:11:\"secrect_key\";s:29:\"gougucmsxxxxhumabcxxxxjixxxng\";s:8:\"cert_url\";s:13:\"/extend/cert/\";s:12:\"pay_back_url\";s:42:\"https://www.gougucms.com/wxappv1/wx/notify\";s:9:\"xcx_appid\";s:18:\"wxdf96xxxx9cd6f0c5\";s:13:\"xcx_appsecret\";s:28:\"gougucmsxxxxhunangdmabcxxxng\";}', 1, 1612522314, 1613789058);
INSERT INTO `cms_config` VALUES (4, 'Api Token配置', 'token', 'a:5:{s:2:\"id\";s:1:\"5\";s:3:\"iss\";s:16:\"www.gougucms.com\";s:3:\"aud\";s:8:\"gougucms\";s:7:\"secrect\";s:8:\"GOUGUCMS\";s:7:\"exptime\";s:4:\"3600\";}', 1, 1627313142, 1627376290);
INSERT INTO `cms_config` VALUES (5, '其他配置', 'other', 'a:4:{s:2:\"id\";s:1:\"5\";s:6:\"author\";s:15:\"勾股工作室\";s:7:\"version\";s:7:\"v2.0.16\";s:6:\"editor\";s:1:\"1\";}', 1, 1613725791, 1645107069);

-- ----------------------------
-- Table structure for cms_department
-- ----------------------------
DROP TABLE IF EXISTS `cms_department`;
CREATE TABLE `cms_department`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '部门名称',
  `pid` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '上级部门id',
  `leader_id` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '部门负责人ID',
  `phone` varchar(60) NOT NULL DEFAULT '' COMMENT '部门联系电话',
  `remark` varchar(1000) NULL DEFAULT '' COMMENT '备注',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态：-1删除 0禁用 1启用',
  `create_time` int(11) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COMMENT = '部门组织';

-- ----------------------------
-- Records of cms_department
-- ----------------------------
INSERT INTO `cms_department` VALUES ('1', '天台县外国语学校', '0', '0', '13688888888', '', '1', '0', '1703814371');
INSERT INTO `cms_department` VALUES ('2', '小学部', '1', '0', '13688888889', '', '1', '0', '1703814381');
INSERT INTO `cms_department` VALUES ('3', '语文', '2', '0', '13688888898', '', '1', '0', '1703814556');
INSERT INTO `cms_department` VALUES ('4', '数学', '2', '0', '13688888898', '', '1', '0', '1703814567');
INSERT INTO `cms_department` VALUES ('5', '体音美', '2', '0', '13688888978', '', '1', '0', '1703814581');
INSERT INTO `cms_department` VALUES ('6', '初中部', '1', '0', '13688889868', '', '1', '0', '1703814518');
INSERT INTO `cms_department` VALUES ('7', '行政后勤', '1', '0', '13688898858', '', '1', '0', '1703814533');
INSERT INTO `cms_department` VALUES ('9', '语文', '6', '0', '13688998838', '', '1', '0', '1703814590');
INSERT INTO `cms_department` VALUES ('10', '科学', '6', '0', '13688999828', '', '1', '0', '1703814599');
INSERT INTO `cms_department` VALUES ('11', '高中部', '1', '0', '13688999918', '', '1', '0', '1703814403');
INSERT INTO `cms_department` VALUES ('16', '财务处', '7', '0', '', '', '1', '0', '0');
INSERT INTO `cms_department` VALUES ('17', '总务处', '7', '0', '', '', '1', '0', '0');
INSERT INTO `cms_department` VALUES ('18', '文科组', '11', '0', '', '', '1', '0', '0');
INSERT INTO `cms_department` VALUES ('19', '理科组', '11', '0', '', '', '1', '0', '0');

-- ----------------------------
-- Table structure for cms_position
-- ----------------------------
DROP TABLE IF EXISTS `cms_position`;
CREATE TABLE `cms_position`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '岗位名称',
  `work_price` int(10) NOT NULL DEFAULT 0 COMMENT '工时单价',
  `remark` varchar(1000) NULL DEFAULT '' COMMENT '备注',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态：-1删除 0禁用 1启用',
  `create_time` int(11) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COMMENT = '岗位职称';

-- ----------------------------
-- Records of cms_position
-- ----------------------------
INSERT INTO `cms_position` VALUES (1, '董事长', 1000, '董事长', 1, 0, 0);
INSERT INTO `cms_position` VALUES (2, '人事总监', 1000, '人事部的最大领导', 1, 0, 0);
INSERT INTO `cms_position` VALUES (3, '普通员工', 500, '普通员工', 1, 0, 0);

-- ----------------------------
-- Table structure for `cms_keywords`
-- ----------------------------
DROP TABLE IF EXISTS `cms_keywords`;
CREATE TABLE `cms_keywords` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '关键字名称',
  `sort` int(11)  NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：-1删除 0禁用 1启用',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET = utf8mb4 COMMENT='关键字表';
-- ----------------------------
-- Records of cms_keywords
-- ----------------------------
INSERT INTO `cms_keywords` VALUES (1, '勾股CMS', 0, 1, 1610183567, 1610184824);
INSERT INTO `cms_keywords` VALUES (2, '勾股BLOG', 0, 1, 1610183567, 1610184824);
INSERT INTO `cms_keywords` VALUES (3, '勾股OA', 0, 1, 1610183567, 1610184824);
INSERT INTO `cms_keywords` VALUES (4, '勾股dev', 0, 1, 1610183567, 1610184824);

-- ----------------------------
-- Table structure for cms_sitemap_cate
-- ----------------------------
DROP TABLE IF EXISTS `cms_sitemap_cate`;
CREATE TABLE `cms_sitemap_cate`  (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '分类名称',
  `status` int(1) NOT NULL DEFAULT 1 COMMENT '1可用-1禁用',
  `sort` int(11) NOT NULL DEFAULT 0 COMMENT '排序',
  `create_time` int(11) NOT NULL DEFAULT 0,
  `update_time` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET = utf8mb4 COMMENT = '网站地图分类表';

-- ----------------------------
-- Table structure for cms_sitemap
-- ----------------------------
DROP TABLE IF EXISTS `cms_sitemap`;
CREATE TABLE `cms_sitemap`  (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sitemap_cate_id` int(11) NOT NULL DEFAULT 0 COMMENT '分类id',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '标题',
  `pc_img` varchar(255) NULL DEFAULT NULL COMMENT 'pc端图片',
  `pc_src` varchar(255) NULL DEFAULT NULL COMMENT 'pc端链接',
  `mobile_img` varchar(255) NULL DEFAULT NULL COMMENT '移动端图片',
  `mobile_src` varchar(255) NULL DEFAULT NULL COMMENT '移动端链接',
  `status` int(1) NOT NULL DEFAULT 1 COMMENT '1可用-1禁用',
  `sort` int(11) NOT NULL DEFAULT 0 COMMENT '排序',
  `create_time` int(11) NOT NULL DEFAULT 0,
  `update_time` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET = utf8mb4 COMMENT = '网站地图内容表';


-- ----------------------------
-- Table structure for `cms_nav`
-- ----------------------------
DROP TABLE IF EXISTS `cms_nav`;
CREATE TABLE `cms_nav` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '标识',
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '1可用-1禁用',
  `desc` varchar(1000) DEFAULT NULL,
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET = utf8mb4 COMMENT='导航';

-- -----------------------------
-- Records of `cms_nav`
-- -----------------------------
INSERT INTO `cms_nav` VALUES ('1', '主导航', 'NAV_HOME', '1', '平台主导航', '0', '0');

-- ----------------------------
-- Table structure for `cms_nav_info`
-- ----------------------------
DROP TABLE IF EXISTS `cms_nav_info`;
CREATE TABLE `cms_nav_info` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `nav_id` int(11) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) DEFAULT '',
  `src` varchar(255) DEFAULT NULL,
  `param` varchar(255) DEFAULT NULL,
  `target` int(1) NOT NULL DEFAULT '0' COMMENT '是否新窗口打开,默认0,1新窗口打开',
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '1可用,-1禁用',
  `sort` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET = utf8mb4 COMMENT='导航详情表';

-- -----------------------------
-- Records of `cms_nav_info`
-- -----------------------------
INSERT INTO `cms_nav_info` VALUES ('1', '0', '1', '首页', '/', 'index', '0', '1', '1', '0', '0');
INSERT INTO `cms_nav_info` VALUES ('4', '0', '1', 'API接口', '/api/index', '', '1', '1', '4', '0', '0');
INSERT INTO `cms_nav_info` VALUES ('6', '0', '1', '后台管理', '/admin/index/index.html', '', '1', '1', '6', '0', '0');
INSERT INTO `cms_nav_info` VALUES ('7', '2', '1', '勾股BLOG', 'https://blog.gougucms.com/home/book/detail/bid/2.html', '', '1', '1', '7', '0', '0');
INSERT INTO `cms_nav_info` VALUES ('8', '2', '1', '勾股OA办公系统', 'https://www.gougucms.com/home/pages/detail/s/gouguoa.html', '', '1', '1', '8', '0', '0');
INSERT INTO `cms_nav_info` VALUES ('9', '2', '1', '勾股DEV项目管理系统', 'https://www.gougucms.com/home/pages/detail/s/gougudev.html', '', '1', '1', '9', '0', '0');
INSERT INTO `cms_nav_info` VALUES ('11', '0', '1', '抽奖', '/admin/lottery/lottery_page', '', '0', '1', '1', '0', '0');
INSERT INTO `cms_nav_info` VALUES ('13', '0', '1', '抽奖签到', '/home/lottery/sign', '', '0', '1', '2', '0', '0');



-- ----------------------------
-- Table structure for `cms_slide`
-- ----------------------------
DROP TABLE IF EXISTS `cms_slide`;
CREATE TABLE `cms_slide` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '标识',
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '1可用-1禁用',
  `desc` varchar(1000) DEFAULT NULL,
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET = utf8mb4 COMMENT='幻灯片表';

-- ----------------------------
-- Records of cms_slide
-- ----------------------------
INSERT INTO `cms_slide` VALUES ('1', '首页轮播', 'INDEX_SLIDE', '1', '首页轮播组。', '0', '0');

-- ----------------------------
-- Table structure for `cms_slide_info`
-- ----------------------------
DROP TABLE IF EXISTS `cms_slide_info`;
CREATE TABLE `cms_slide_info` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slide_id` int(11) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `desc` varchar(1000) DEFAULT NULL,
  `img` varchar(255) NOT NULL DEFAULT '',
  `src` varchar(255) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '1可用-1禁用',
  `sort` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET = utf8mb4 COMMENT='幻灯片详情表';

-- ----------------------------
-- Table structure for cms_links
-- ----------------------------
DROP TABLE IF EXISTS `cms_links`;
CREATE TABLE `cms_links`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255)  NOT NULL DEFAULT '' COMMENT '网站标题',
  `logo` int(11) NOT NULL DEFAULT 0 COMMENT '网站logo',
  `src` varchar(255) NULL DEFAULT NULL COMMENT '链接',
  `target` int(1) NOT NULL DEFAULT 1 COMMENT '是否新窗口打开，1是,0否',
  `status` int(1) NOT NULL DEFAULT 1 COMMENT '状态:1可用-1禁用',
  `sort` int(11) NOT NULL DEFAULT 0 COMMENT '排序',
  `create_time` int(11) NOT NULL DEFAULT 0,
  `update_time` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB DEFAULT CHARACTER SET = utf8mb4 COMMENT = '友情链接';

-- ----------------------------
-- Records of cms_links
-- ----------------------------
INSERT INTO `cms_links` VALUES (1, '勾股CMS', 0, 'https://www.gougucms.com', 1, 1, 1, 1624516962, 1624517078);
INSERT INTO `cms_links` VALUES (2, '勾股BLOG', 0, 'https://blog.gougucms.com', 0, 1, 2, 1624517262, 1624517178);
INSERT INTO `cms_links` VALUES (3, '勾股OA', 0, 'https://oa.gougucms.com', 0, 1, 3, 1624517262, 1624517178);
INSERT INTO `cms_links` VALUES (4, '勾股DEV', 0, 'https://dev.gougucms.com', 0, 1, 4, 1624517262, 1624517178);

-- ----------------------------
-- Table structure for cms_search_keywords
-- ----------------------------
DROP TABLE IF EXISTS `cms_search_keywords`;
CREATE TABLE `cms_search_keywords`  (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '关键字',
  `times` int(11) NOT NULL DEFAULT 1 COMMENT '搜索次数',
  `type` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1,2',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET = utf8mb4 COMMENT = '搜索关键字表';

-- ----------------------------
-- Records of cms_search_keywords
-- ----------------------------
INSERT INTO `cms_search_keywords` VALUES (1, '勾股CMS', 1, 1);
INSERT INTO `cms_search_keywords` VALUES (2, '勾股OA', 1, 1);
INSERT INTO `cms_search_keywords` VALUES (3, '勾股DEV', 1, 1);
INSERT INTO `cms_search_keywords` VALUES (4, '勾股BLOG', 1, 1);
INSERT INTO `cms_search_keywords` VALUES (5, '勾股Admin', 1, 1);
INSERT INTO `cms_search_keywords` VALUES (6, '勾股UI', 1, 1);

-- ----------------------------
-- Table structure for cms_user_level
-- ----------------------------
DROP TABLE IF EXISTS `cms_user_level`;
CREATE TABLE `cms_user_level`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '等级名称',
  `desc` varchar(1000) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 1 COMMENT '状态:0禁用,1正常',
  `create_time` int(11) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COMMENT = '会员等级表';

-- ----------------------------
-- Records of cms_admin_module
-- ----------------------------
INSERT INTO `cms_user_level` VALUES (1, '普通会员','', 1, 1639562910, 0);
INSERT INTO `cms_user_level` VALUES (2, '铜牌会员','', 1, 1639562910, 0);
INSERT INTO `cms_user_level` VALUES (3, '银牌会员','', 1, 1639562910, 0);
INSERT INTO `cms_user_level` VALUES (4, '黄金会员','', 1, 1639562910, 0);
INSERT INTO `cms_user_level` VALUES (5, '白金会员','', 1, 1639562910, 0);
INSERT INTO `cms_user_level` VALUES (6, '钻石会员','', 1, 1639562910, 0);

-- ----------------------------
-- Table structure for cms_user
-- ----------------------------
DROP TABLE IF EXISTS `cms_user`;
CREATE TABLE `cms_user`  (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `nickname` varchar(255) NOT NULL DEFAULT '' COMMENT '用户微信昵称',
  `nickname_a` varchar(255) NOT NULL DEFAULT '' COMMENT '用户微信昵称16进制',
  `username` varchar(100) NOT NULL DEFAULT '' COMMENT '账号',
  `password` varchar(100) NOT NULL DEFAULT '' COMMENT '密码',
  `salt` varchar(100) NOT NULL DEFAULT '' COMMENT '密码盐',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '真实姓名',
  `mobile` varchar(20) NOT NULL DEFAULT '' COMMENT '手机（也可以作为登录账号)',
  `mobile_status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '手机绑定状态： 0未绑定 1已绑定',
  `email` varchar(50) NOT NULL DEFAULT '' COMMENT '邮箱',
  `headimgurl` varchar(255) NOT NULL DEFAULT '' COMMENT '微信头像',
  `sex` tinyint(1) NOT NULL DEFAULT 0 COMMENT '性别 0:未知 1:女 2:男 ',    
  `desc` varchar(1000) NOT NULL DEFAULT '' COMMENT '个人简介',
  `birthday` int(11) NULL DEFAULT '0' COMMENT '生日',
  `country` varchar(20) NOT NULL DEFAULT '' COMMENT '国家',
  `province` varchar(20) NOT NULL DEFAULT '' COMMENT '省',
  `city` varchar(20) NOT NULL DEFAULT '' COMMENT '城市',  
  `company` varchar(100) NOT NULL DEFAULT '' COMMENT '公司',  
  `address` varchar(100) NOT NULL DEFAULT '' COMMENT '公司地址',
  `depament` varchar(20) NOT NULL DEFAULT '' COMMENT '部门',
  `position` varchar(20) NOT NULL DEFAULT '' COMMENT '职位',
  `puid` int(11) NOT NULL DEFAULT 0 COMMENT '推荐人ID,默认是0',
  `qrcode_invite` int(11) NOT NULL DEFAULT 0 COMMENT '邀请场景二维码id',  
  `level` tinyint(1) NOT NULL DEFAULT 1 COMMENT '等级  默认是普通会员',   
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态  -1删除 0禁用 1正常',   
  `last_login_time` int(11) NOT NULL DEFAULT '0' COMMENT '最后登录时间',
  `last_login_ip` varchar(64) NOT NULL DEFAULT '' COMMENT '最后登录IP',
  `login_num` int(11) NOT NULL DEFAULT '0',
  `register_time` int(11) NOT NULL DEFAULT '0' COMMENT '注册时间',
  `register_ip` varchar(64) NOT NULL DEFAULT '' COMMENT '注册IP',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '信息更新时间',
  `wx_platform` int(11) NOT NULL DEFAULT 0 COMMENT '首次注册来自于哪个微信平台',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARACTER SET = utf8mb4 COMMENT = '用户表';

-- ----------------------------
-- Records of for `cms_user`
-- ----------------------------
INSERT INTO `cms_user` VALUES (1, '勾股CMS', '', 'hdm58', '7aba99e08564eb6a9a6038255aeb265c', '03K6PWjT2dAFBsa8oJYZ', '小明名', '13589858989', 0, 'hdm58@qq.com', '/static/admin/images/icon.png', 0, '勾股科技', 1627401600, '', '', '广州', '勾股科技', '珠江新城', '技术部', '技术总监', 0, 0, 1, 1, 1645009233, '163.142.175.169', 7, 1627457646, '163.142.247.150', 0, 0);


-- ----------------------------
-- Table structure for `cms_user_log`
-- ----------------------------
DROP TABLE IF EXISTS `cms_user_log`;
CREATE TABLE `cms_user_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `uid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `nickname` varchar(255) NOT NULL DEFAULT '' COMMENT '昵称',
  `type` varchar(80) NOT NULL DEFAULT '' COMMENT '操作类型',
  `title` varchar(80) NOT NULL DEFAULT '' COMMENT '操作标题',
  `content` text COMMENT '操作描述',
  `module` varchar(32) NOT NULL DEFAULT '' COMMENT '模块',
  `controller` varchar(32) NOT NULL DEFAULT '' COMMENT '控制器',
  `function` varchar(32) NOT NULL DEFAULT '' COMMENT '方法',
  `ip` varchar(64) NOT NULL DEFAULT '' COMMENT '登录ip',
  `param_id` int(11) unsigned NOT NULL COMMENT '操作ID',
  `param` text COMMENT '参数json格式',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0删除 1正常',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET = utf8mb4 COMMENT='用户操作日志表';

-- ----------------------------
-- Table structure for cms_file_group
-- ----------------------------
DROP TABLE IF EXISTS `cms_file_group`;
CREATE TABLE `cms_file_group`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '分组名',
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建人',
  `create_time` int(11) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(11) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COMMENT = '文件分组表';

-- ----------------------------
-- Table structure for cms_file
-- ----------------------------
DROP TABLE IF EXISTS `cms_file`;
CREATE TABLE `cms_file`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `module` varchar(15) NOT NULL DEFAULT '' COMMENT '所属模块',
  `sha1` varchar(60) NOT NULL COMMENT 'sha1',
  `md5` varchar(60) NOT NULL COMMENT 'md5',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '原始文件名',
  `filename` varchar(255) NOT NULL DEFAULT '' COMMENT '文件名',
  `filepath` varchar(255) NOT NULL DEFAULT '' COMMENT '文件路径+文件名',
  `filesize` int(10) NOT NULL DEFAULT 0 COMMENT '文件大小',
  `fileext` varchar(10) NOT NULL DEFAULT '' COMMENT '文件后缀',
  `mimetype` varchar(100) NOT NULL DEFAULT '' COMMENT '文件类型',
  `group_id` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '文件分组ID',
  `user_id` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '上传会员ID',
  `uploadip` varchar(15) NOT NULL DEFAULT '' COMMENT '上传IP',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0未审核1已审核-1不通过',
  `create_time` int(11) NOT NULL DEFAULT 0,
  `admin_id` int(11) NOT NULL COMMENT '审核者id',
  `delete_time` int(11) NOT NULL DEFAULT 0 COMMENT '删除时间',
  `audit_time` int(11) NOT NULL DEFAULT 0 COMMENT '审核时间',
  `action` varchar(50) NOT NULL DEFAULT '' COMMENT '来源模块功能',
  `use` varchar(255) NULL DEFAULT NULL COMMENT '用处',
  `download` int(11) NOT NULL DEFAULT 0 COMMENT '下载量',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COMMENT = '文件表';

-- ----------------------------
-- Records of cms_file
-- ----------------------------
INSERT INTO `cms_file` VALUES (1, 'admin', '5125347886f07f48f7003825660117039eb8784f', '563e5e8f48e607ed54461796b0cb4844', 'f95982689eb222b84e999122a50b3780.jpg.jpg', 'f95982689eb222b84e999122a50b3780.jpg', 'https://blog.gougucms.com/storage/202202/f95982689eb222b84e999122a50b3780.jpg', 62609, 'jpg', 'image/jpeg', 0, 1, '127.0.0.1', 1, 1645057433, 1, 0, 1645057433, 'upload', 'thumb', 0);
INSERT INTO `cms_file` VALUES (2, 'admin', '5125347886f07f48f7003825660117039eb8784f', '563e5e8f48e607ed54461796b0cb4844', 'e729477de18e3be7e7eb4ec7fe2f821e.jpg', 'e729477de18e3be7e7eb4ec7fe2f821e.jpg', 'https://blog.gougucms.com/storage/202202/e729477de18e3be7e7eb4ec7fe2f821e.jpg', 62609, 'jpg', 'image/jpeg', 0, 1, '127.0.0.1', 1, 1645057433, 1, 0, 1645057433, 'upload', 'thumb', 0);
INSERT INTO `cms_file` VALUES (3, 'admin', '5125347886f07f48f7003825660117039eb8784f', '563e5e8f48e607ed54461796b0cb4844', '1193f7a1585b9f6e8a97ae17718018b3.jpg', 'images/1193f7a1585b9f6e8a97ae17718018b3.jpg', 'https://blog.gougucms.com/storage/202204/1193f7a1585b9f6e8a97ae17718018b3.jpg', 62609, 'jpg', 'image/jpeg', 0, 1, '127.0.0.1', 1, 1645057433, 1, 0, 1645057433, 'upload', 'thumb', 0);
INSERT INTO `cms_file` VALUES (4, 'admin', '5125347886f07f48f7003825660117039eb8784f', '563e5e8f48e607ed54461796b0cb4844', '0f22a5ba4797b2fa22049ea73e6f779c.jpg', 'images/0f22a5ba4797b2fa22049ea73e6f779c.jpg', 'https://blog.gougucms.com/storage/202202/0f22a5ba4797b2fa22049ea73e6f779c.jpg', 62609, 'jpg', 'image/jpeg', 0, 1, '127.0.0.1', 1, 1645057433, 1, 0, 1645057433, 'upload', 'thumb', 0);


-- ----------------------------
-- Table structure for cms_gallery_cate
-- ----------------------------
DROP TABLE IF EXISTS `cms_gallery_cate`;
CREATE TABLE `cms_gallery_cate`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '图集分类名称',
  `sort` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '排序',
  `pid` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '上级id',
  `keywords` varchar(255) DEFAULT '' COMMENT '关键字',
  `desc` varchar(1000) DEFAULT '' COMMENT '描述',
  `create_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COMMENT = '图集分类::crud';

-- ----------------------------
-- Records of cms_gallery_cate
-- ----------------------------
INSERT INTO `cms_gallery_cate` VALUES (1, '勾股图集', 1, 0, '勾股CMS', '左手自研，右手开源', 1645058420, 1645058420, 0);

-- ----------------------------
-- Table structure for cms_gallery
-- ----------------------------
DROP TABLE IF EXISTS `cms_gallery`;
CREATE TABLE `cms_gallery`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cate_id` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '分类ID',
  `type` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '属性:1精华,2热门,3推荐',
  `is_home` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否首页显示:0否,1是',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态:0下架 1正常',
  `title` varchar(200) NOT NULL DEFAULT '' COMMENT '图集名称',
  `thumb` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '缩略图',
  `desc` varchar(1000) NOT NULL DEFAULT '' COMMENT '图集摘要',
  `content` text NULL COMMENT '内容',
  `user_id` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户id',
  `origin` varchar(255) NOT NULL DEFAULT '' COMMENT '来源或作者',
  `origin_url` varchar(255) NOT NULL DEFAULT '' COMMENT '来源地址',
  `read` int(11) NOT NULL DEFAULT '0' COMMENT '阅读量',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `admin_id` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建人',
  `create_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COMMENT = '图集::crud';

-- ----------------------------
-- Records of cms_gallery
-- ----------------------------
INSERT INTO `cms_gallery` VALUES (1, 1, 3, 1, 1, '勾股开源系列系统', 1, '勾股科技专注于CMS系统，OA办公系统，CRM系统，ERP系统，在线教育系统，项目管理系统等系统软件的建设。左手研发，右手开源，未来可期！', NULL, 0, '勾股科技', 'http://www.gougucms.com', 0, 0, 1, 1655452531, 1655633932, 0);

-- ----------------------------
-- Table structure for `cms_gallery_keywords`
-- ----------------------------
DROP TABLE IF EXISTS `cms_gallery_keywords`;
CREATE TABLE `cms_gallery_keywords` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `aid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '图集ID',
  `keywords_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '关联关键字id',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `aid` (`aid`)
) ENGINE=InnoDB DEFAULT CHARACTER SET = utf8mb4 COMMENT='图集关联表';

-- ----------------------------
-- Records of cms_gallery_keywords
-- ----------------------------
INSERT INTO `cms_gallery_keywords` VALUES (1, 1, 1, 1644823517);

-- ----------------------------
-- Table structure for `cms_gallery_file`
-- ----------------------------
DROP TABLE IF EXISTS `cms_gallery_file`;
CREATE TABLE `cms_gallery_file` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `aid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '图集ID',
  `file_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件id',
  `name` varchar(200) NOT NULL DEFAULT '' COMMENT '图片名称',
  `title` varchar(200) NOT NULL DEFAULT '' COMMENT '标题',
  `desc` varchar(1000) NOT NULL DEFAULT '' COMMENT '摘要',
  `filepath` varchar(200) NOT NULL DEFAULT '' COMMENT '图片路径',
  `link` varchar(200) NOT NULL DEFAULT '' COMMENT '链接地址',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `aid` (`aid`)
) ENGINE=InnoDB DEFAULT CHARACTER SET = utf8mb4 COMMENT='图集关联表';

-- ----------------------------
-- Records of cms_gallery_file
-- ----------------------------
INSERT INTO `cms_gallery_file` VALUES (1, 1, 0, '勾股cms', '勾股CMS', '勾股CMS是一套基于ThinkPHP6 + Layui + MySql打造的轻量级的通用后台管理框架。简单、易用且方便二次开发，是一款免费可商用的开源项目', 'https://blog.gougucms.com/storage/202202/f95982689eb222b84e999122a50b3780.jpg', 'http://www.gougucms.com', 1, 1655633932);
INSERT INTO `cms_gallery_file` VALUES (2, 1, 0, '勾股OA', '勾股OA', '勾股OA是一款实用的企业办公系统框架。集成了系统设置、人事管理、消息管理、审批管理、日常办公、财务管理等基础模块。系统简约，易于功能扩展，方便二次开发，让开发者更专注于业务深度需求的开发，通过二次开发之后可以用来做CRM，ERP，业务管理等系统。', 'https://blog.gougucms.com/storage/202202/e729477de18e3be7e7eb4ec7fe2f821e.jpg', 'http://oa.gougucms.com', 2, 1655633932);
INSERT INTO `cms_gallery_file` VALUES (3, 1, 0, '勾股DEV', '勾股DEV', '勾股DEV是一款任务管理与团队协作的任务/项目管理系统，可以进行工作任务计划、执行、管理，完整地覆盖了项目管理的核心流程。项目基于GPL-3.0协议开源发布。', 'https://blog.gougucms.com/storage/202204/1193f7a1585b9f6e8a97ae17718018b3.jpg', 'http://dev.gougucms.com', 3, 1655633932);

-- ----------------------------
-- Table structure for cms_goods_cate
-- ----------------------------
DROP TABLE IF EXISTS `cms_goods_cate`;
CREATE TABLE `cms_goods_cate`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '分类名称',
  `sort` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '排序',
  `pid` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '上级id',
  `keywords` varchar(255) DEFAULT '' COMMENT '关键字',
  `desc` varchar(1000) DEFAULT '' COMMENT '描述',
  `create_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  `delete_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COMMENT = '商品分类::crud';

-- ----------------------------
-- Records of cms_goods_cate
-- ----------------------------
INSERT INTO `cms_goods_cate` VALUES (1, '勾股科技', 1, 0, '勾股CMS', '左手自研，右手开源', 1645058420, 0, 0);

-- ----------------------------
-- Table structure for cms_goods
-- ----------------------------
DROP TABLE IF EXISTS `cms_goods`;
CREATE TABLE `cms_goods`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cate_id` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '分类ID',
  `type` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '属性:1精华,2热门,3推荐',
  `is_home` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否首页显示:0否,1是',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '商品状态:0下架,1正常',
  `title` varchar(200) NOT NULL DEFAULT '' COMMENT '商品名称',
  `thumb` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '缩略图',
  `banner` varchar(1000) NOT NULL DEFAULT '' COMMENT '商品轮播图',
  `tips` varchar(255) NOT NULL DEFAULT '' COMMENT '商品卖点，一句话推销',
  `desc` varchar(1000) NOT NULL DEFAULT '' COMMENT '商品摘要',
  `content` text NOT NULL COMMENT '内容',
  `base_price` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '市场价格',
  `price` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '实际价格',
  `stocks` int(11) NOT NULL DEFAULT 0 COMMENT '商品库存',
  `sales` int(11) NOT NULL DEFAULT 0 COMMENT '商品销量',
  `address` varchar(200) NOT NULL DEFAULT '' COMMENT '商品发货地址',
  `start_time` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '开始抢购时间',
  `end_time` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '结束抢购时间',
  `read` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '阅读量',
  `sort` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '排序',
  `is_mail` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '是否包邮:0否,1是',
  `tag_values` varchar(200) NOT NULL DEFAULT '' COMMENT '商品标签:1正品保证,2一年保修,3七天退换,4赠运费险,5闪电发货,6售后无忧',
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建人',
  `create_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '编辑时间',
  `delete_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COMMENT = '商品::crud';

-- ----------------------------
-- Records of cms_goods
-- ----------------------------
INSERT INTO `cms_goods` VALUES (1, 1, 1, 1, 1, '勾股CMS，勾股Blog，勾股OA定制开发方案', 1, '', '赠送一年免费维护服务', '','勾股CMS定制开发方案99元起，勾股Blog定制开发方案199元起，勾股OA定制开发方案299元起。欢迎联系QQ:327725426咨询定制 。</p>', 199.00, 99.00, 0, 0, '', 0, 0, 0, 1, 1, '1,2,6', 0, 1644823517, 0,0);

-- ----------------------------
-- Table structure for `cms_goods_keywords`
-- ----------------------------
DROP TABLE IF EXISTS `cms_goods_keywords`;
CREATE TABLE `cms_goods_keywords` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `aid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商品ID',
  `keywords_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '关联关键字id',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态:-1删除 0禁用 1启用',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `aid` (`aid`),
  KEY `inid` (`keywords_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET = utf8mb4 COMMENT='商品关联表';

-- ----------------------------
-- Records of cms_goods_keywords
-- ----------------------------
INSERT INTO `cms_goods_keywords` VALUES (1, 1, 1, 1, 1644823517);

-- ----------------------------
-- Table structure for cms_pages
-- ----------------------------
DROP TABLE IF EXISTS `cms_pages`;
CREATE TABLE `cms_pages`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL DEFAULT '' COMMENT '页面名称',
  `thumb` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '缩略图',
  `banner` varchar(1000) NOT NULL DEFAULT '' COMMENT '图集相册',
  `desc` varchar(1000) NOT NULL DEFAULT '' COMMENT '页面摘要',
  `content` text NOT NULL COMMENT '内容',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '页面状态:0下架,1正常',
  `read` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '阅读量',
  `sort` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '排序',
  `name` varchar(200) NOT NULL DEFAULT '' COMMENT 'url文件名',
  `template` varchar(200) NOT NULL DEFAULT '' COMMENT '前端模板',
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建人',
  `create_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '编辑时间',
  `delete_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COMMENT = '单页面::crud';

-- ----------------------------
-- Records of cms_pages
-- ----------------------------
INSERT INTO `cms_pages` VALUES (1, '关于我们', 1, '', '勾股CMS是一套基于ThinkPHP6+Layui+MySql打造的轻量级、高性能快速建站的内容管理系统。后台管理模块，一目了然，操作简单，通用型后台权限管理框架，开箱即用。', '<p>勾股CMS是一套基于ThinkPHP6 + Layui + MySql打造的轻量级、高性能极速后台开发框架。通用型的后台权限管理框架，操作简单、开箱即用。系统易于功能扩展，代码维护，方便二次开发，让开发者更专注于业务深度需求的开发，帮助开发者简单高效降低二次开发成本。</p>\n<p><img src=\"https://blog.gougucms.com/storage/image/202109/1630457997502502.png\" /></p>', 1,1, 0, '', 'default', 1, 1653984295, 1653993466,0);
INSERT INTO `cms_pages` VALUES (2, '勾股OA办公系统', 0, '', '勾股OA是一款实用的企业办公系统框架。集成了系统设置、人事管理、消息管理、审批管理、日常办公、财务管理等基础模块。系统简约，易于功能扩展，方便二次开发，让开发者更专注于业务深度需求的开发，通过二次开发之后可以用来做CRM，ERP，业务管理等系统。', '<p>勾股OA是一款实用的企业办公系统框架。集成了系统设置、人事管理、消息管理、审批管理、日常办公、财务管理等基础模块。系统简约，易于功能扩展，方便二次开发，让开发者更专注于业务深度需求的开发，通过二次开发之后可以用来做CRM，ERP，业务管理等系统。</p>', 1,9, 0, 'gouguoa', 'oa', 1, 1654419120, 0,0);
INSERT INTO `cms_pages` VALUES (3,  '勾股DEV项目管理系统', 0, '', '勾股DEV是一款专为IT研发团队打造的项目管理与团队协作的系统工具，可以在线管理团队的工作、项目和任务，覆盖从需求提出到研发完成上线整个过程的项目协作。', '<p >勾股DEV是一款专为IT研发团队打造的项目管理与团队协作的系统工具，可以在线管理团队的工作、项目和任务，覆盖从需求提出到研发完成上线整个过程的项目协作。</p>\n<p>勾股DEV的产品理念：通过&ldquo;项目（Project）&rdquo;的形式把成员、需求、任务、缺陷(BUG)、文档、互动讨论以及各种形式的资源组织在一起，团队成员参与更新任务、文档等内容来推动项目的进度，同时系统利用时间线索和各种动态的报表的形式来自动给成员汇报项目进度。</p>', 1,8, 0, 'gougudev', 'dev', 1, 1654167139, 0,0);

-- ----------------------------
-- Table structure for `cms_pages_keywords`
-- ----------------------------
DROP TABLE IF EXISTS `cms_pages_keywords`;
CREATE TABLE `cms_pages_keywords` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `aid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '页面ID',
  `keywords_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '关联关键字id',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：-1删除 0禁用 1启用',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `aid` (`aid`),
  KEY `inid` (`keywords_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET = utf8mb4 COMMENT='单页面关联表';

-- ----------------------------
-- Records of cms_pages_keywords
-- ----------------------------
INSERT INTO `cms_pages_keywords` VALUES (1, 1, 1, 1, 1644823517);
INSERT INTO `cms_pages_keywords` VALUES (2, 2, 2, 1, 1644823517);
INSERT INTO `cms_pages_keywords` VALUES (3, 3, 3, 1, 1644823517);

-- ----------------------------
-- Table structure for `cms_lottery_avatar`
-- ----------------------------
DROP TABLE IF EXISTS `cms_lottery_avatar`;
CREATE TABLE `cms_lottery_avatar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'url',
  `thumb_url` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '缩略图url',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of cms_lottery_avatar
-- ----------------------------
INSERT INTO `cms_lottery_avatar` VALUES ('1', '/Avatar/1.png', '/Avatar/1_100.png');
INSERT INTO `cms_lottery_avatar` VALUES ('2', '/Avatar/2.png', '/Avatar/2_100.png');
INSERT INTO `cms_lottery_avatar` VALUES ('3', '/Avatar/3.png', '/Avatar/3_100.png');
INSERT INTO `cms_lottery_avatar` VALUES ('4', '/Avatar/4.png', '/Avatar/4_100.png');
INSERT INTO `cms_lottery_avatar` VALUES ('5', '/Avatar/5.png', '/Avatar/5_100.png');
INSERT INTO `cms_lottery_avatar` VALUES ('6', '/Avatar/6.png', '/Avatar/6_100.png');
INSERT INTO `cms_lottery_avatar` VALUES ('7', '/Avatar/7.png', '/Avatar/7_100.png');
INSERT INTO `cms_lottery_avatar` VALUES ('8', '/Avatar/8.png', '/Avatar/8_100.png');
INSERT INTO `cms_lottery_avatar` VALUES ('9', '/Avatar/9.png', '/Avatar/9_100.png');
INSERT INTO `cms_lottery_avatar` VALUES ('10', '/Avatar/10.png', '/Avatar/10_100.png');
INSERT INTO `cms_lottery_avatar` VALUES ('11', '/Avatar/11.png', '/Avatar/11_100.png');
INSERT INTO `cms_lottery_avatar` VALUES ('12', '/Avatar/12.png', '/Avatar/12_100.png');
INSERT INTO `cms_lottery_avatar` VALUES ('13', '/Avatar/13.png', '/Avatar/13_100.png');
INSERT INTO `cms_lottery_avatar` VALUES ('14', '/Avatar/14.png', '/Avatar/14_100.png');
INSERT INTO `cms_lottery_avatar` VALUES ('15', '/Avatar/15.png', '/Avatar/15_100.png');
INSERT INTO `cms_lottery_avatar` VALUES ('16', '/Avatar/16.png', '/Avatar/16_100.png');

-- ----------------------------
-- Table structure for `cms_lottery_award_goods`
-- ----------------------------
DROP TABLE IF EXISTS `cms_lottery_award_goods`;
CREATE TABLE `cms_lottery_award_goods` (
  `id` int(5) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `award_level` varchar(10) NOT NULL DEFAULT '' COMMENT '奖次',
  `winner_number` int(4) NOT NULL COMMENT '奖品数量',
  `goods_name` varchar(20) NOT NULL DEFAULT '' COMMENT '奖品',
  `limit_number` int(11) DEFAULT '10' COMMENT '单次抽奖人数',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='奖品设置::crud';

-- ----------------------------
-- Records of cms_lottery_award_goods
-- ----------------------------
INSERT INTO `cms_lottery_award_goods` VALUES ('1', '一等奖', '20', 'Iphone6s plus', '5');
INSERT INTO `cms_lottery_award_goods` VALUES ('2', '二等奖', '20', 'Ipad Air2', '10');
INSERT INTO `cms_lottery_award_goods` VALUES ('3', '三等奖', '100', '飞利浦空气炸窝', '20');


-- ----------------------------
-- Table structure for `cms_lottery_config`
-- ----------------------------
DROP TABLE IF EXISTS `cms_lottery_config`;
CREATE TABLE `cms_lottery_config` (
  `id` int(3) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `title` varchar(20) NOT NULL DEFAULT '' COMMENT '标题',
  `name` varchar(20) NOT NULL DEFAULT '' COMMENT '名称',
  `content` varchar(10) NOT NULL DEFAULT '' COMMENT '配置内容',  
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='抽奖设置';

-- ----------------------------
-- Records of cms_lottery_config
-- ----------------------------
INSERT INTO `cms_lottery_config` VALUES ('1', '当前抽奖奖次', 'now_level', '二等奖');
INSERT INTO `cms_lottery_config` VALUES ('2', '签到人数', 'total_number', '260', '');


-- ----------------------------
-- Table structure for `cms_lottery_list`
-- ----------------------------
DROP TABLE IF EXISTS `cms_lottery_list`;
CREATE TABLE `cms_lottery_list` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `real_name` varchar(20) NOT NULL DEFAULT '' COMMENT '姓名',
  `phone_number` varchar(20) NOT NULL DEFAULT '' COMMENT '手机号码',
  `level` varchar(10) NOT NULL COMMENT '获奖等级',
  `lottery_code` int(10) NOT NULL DEFAULT '0' COMMENT '兑奖号',
  `got_award` varchar(10) NOT NULL DEFAULT '' COMMENT '是否已领奖',
  `ctime` datetime NOT NULL COMMENT '中奖时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='中奖名单::crud';

-- ----------------------------
-- Table structure for `cms_lottery_staff`
-- ----------------------------
DROP TABLE IF EXISTS `cms_lottery_staff`;
CREATE TABLE `cms_lottery_staff` (
  `id` int(3) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `real_name` varchar(40) NOT NULL DEFAULT '' COMMENT '姓名',
  `status` varchar(5) NOT NULL DEFAULT '员工' COMMENT '类别',
  `department` varchar(15) NOT NULL DEFAULT '' COMMENT '部门',
  `signed` varchar(5) NOT NULL DEFAULT '否' COMMENT '签到',
  `phone_number` varchar(16) NOT NULL DEFAULT '' COMMENT '手机',
  `level` varchar(10) NOT NULL DEFAULT '' COMMENT '抽奖结果',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='员工::crud';


-- ----------------------------
-- Table structure for `cms_lottery_user`
-- ----------------------------
DROP TABLE IF EXISTS `cms_lottery_user`;
CREATE TABLE `cms_lottery_user` (
  `id` int(5) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `real_name` varchar(20) NOT NULL DEFAULT '' COMMENT '姓名',
  `phone_number` varchar(15) NOT NULL DEFAULT '' COMMENT '手机号码',
  `sign_time` datetime NOT NULL COMMENT '签到时间',
  `avatar_url` varchar(100) NOT NULL COMMENT '头像url',
  `status` varchar(10) NOT NULL DEFAULT '' COMMENT '状态（员工、嘉宾）',
  `lottery_allow` tinyint(1) NOT NULL DEFAULT '1' COMMENT '允许抽奖：1；否则：0',
  `lottery_done` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否已经抽奖',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=525 DEFAULT CHARSET=utf8 COMMENT='抽奖用户::crud';

-- ----------------------------
-- Table structure for `cms_file_download`
-- ----------------------------
DROP TABLE IF EXISTS `cms_file_download`;
CREATE TABLE `cms_file_download` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `filename` varchar(255) NOT NULL DEFAULT '' COMMENT '文件名',
  `filepath` varchar(255) NOT NULL DEFAULT '' COMMENT '文件路径+文件名',
  `fileext` varchar(10) NOT NULL DEFAULT '' COMMENT '文件后缀',
  `mimetype` varchar(100) NOT NULL DEFAULT '' COMMENT '文件类型',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0未审核1已审核-1不通过',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `delete_time` int(11) NOT NULL DEFAULT '0' COMMENT '删除时间',
  `action` varchar(50) NOT NULL DEFAULT '' COMMENT '来源模块功能',
  `download` int(11) NOT NULL DEFAULT '0' COMMENT '下载量',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COMMENT='文件下载::crud';